Higgason Construction is a fully licensed and bonded general contractor and we are proud to provide a range of home remodeling services to residents of Sammamish, Bellevue, Redmond, Issaquah, Mercer Island, Newcastle, Kirkland, and the surrounding areas.

Address: 24846 SE 19th St, Sammamish, WA 98075, USA

Phone: 425-577-8512

Website: https://www.higgasonhomes.com
